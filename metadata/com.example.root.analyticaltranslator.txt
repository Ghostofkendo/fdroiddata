Categories:Science & Education
License:GPLv3
Web Site:https://github.com/xpheres/AndroidAnalyticalTranslator/blob/HEAD/README.md
Source Code:https://github.com/xpheres/AndroidAnalyticalTranslator
Issue Tracker:https://github.com/xpheres/AndroidAnalyticalTranslator/issues
Donate:http://lingoworld.eu/lingoworld/donations/donate.php
Bitcoin:15WVb3LZWCsdZGjkNFBuELwt3U4zpnSgwa

Auto Name:Analytical Translator
Summary:Natural language translator
Description:
Front-end for the natural language translator project [https://github.com/xpheres/analyticaltranslator "Analytical Translator"], a
translator that analyzes sentences, corrects syntax and grammar and reports and
explains mistakes. A pedagogical translator for language learners that detects
and corrects gender,conjugation and declination discrepancies as well as some
orthographic mistakes.

Only translation from German to Spanish is already implemented (English is
being implemented right now).
.

Repo Type:git
Repo:https://github.com/xpheres/AndroidAnalyticalTranslator

Build:0.01,1
    commit=9622285934dbf9fd4497618c43999d673f1ea4e7
    subdir=app
    gradle=yes

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:0.01
Current Version Code:1
