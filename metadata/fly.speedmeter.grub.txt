Categories:Navigation
License:GPLv2
Web Site:https://github.com/flyingrub/SpeedMeter/blob/HEAD/README.md
Source Code:https://github.com/flyingrub/SpeedMeter
Issue Tracker:https://github.com/flyingrub/SpeedMeter/issues
Changelog:
Donate:https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=SP3QEMAM8XZYQ

Auto Name:Speedmeter
Summary:Lightweight speed meter
Description:
Display some useful information about a route:
* Actual speed
* Time elapsed
* M or KM traveled
* Max speed

Older versions are available as [[isn.fly.speedmeter]].
.

Repo Type:git
Repo:https://github.com/flyingrub/SpeedMeter.git

Build:1.2,12
    commit=1.2
    prebuild=mv java src
    target=android-20

Build:2.0 BETA,20
    commit=0de68dec8cae61315852045581531eafecab0781
    subdir=app
    gradle=yes

Auto Update Mode:Version %v
Update Check Mode:Tags
Current Version:1.2
Current Version Code:12

